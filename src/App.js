import React from 'react';
import { withNamespaces } from 'react-i18next';
import Modal from './Modal.js';
import axios from 'axios';
import $ from 'jquery';

$(document).ready(function() {
       $('.header__burger').click(function(event) {
           $('.header__burger,.menu__nav').toggleClass('active');
           $('body').toggleClass('lock');
       });
   });

class App extends React.Component {
  state = {
    show: false,
    name: '',
    email: '',
    phone: '',
    text: ''
  };
  // componentDidMount=()=>{
  //   $(document).ready(function() {
  //     $('.header__burger').click(function(event) {
  //         $('.header__burger,.menu__nav').toggleClass('active');
  //         $('body').toggleClass('lock');
  //     });
  // });
  // }
  handleChangeName = event => {
    this.setState({ name: event.target.value });
  }
  handleChangeEmail = event => {
    this.setState({ email: event.target.value });
  }
  handleChangePhone = event => {
    this.setState({ phone: event.target.value });
  }
  handleChangeText = event => {
    this.setState({ text: event.target.value });
  }
  showModal = e => {
    this.setState({
      show: !this.state.show
    });
  };
  handleSubmit = event => {event.preventDefault(); 
    const Response =
    {
      subject: this.state.name + this.state.phone,
      email: this.state.email,
      text: this.state.text
    }
    axios.post('https://localhost:44307/email', Response)
      .then(res => {
        console.log(res);
        console.log(res.data);
      });
    };


  render() {
    
    const { t, i18n } = this.props;
    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
    };
    
    return (
      <body>

        <div id="what" class="main-wrapper">
          <header class="header">
            <div class="container__header">
              <div class="header__body">
                <div class="menu__logo">
                  <img src={require('./img/Profi-IT_logo.png')} alt="" />
                  <div class="menu__language">
                    <p onClick={() => changeLanguage('ru')}>Русский</p>
                    <div class="menu__langSelect">
                      <ul>
                        <li class="ukr__lang" onClick={() => changeLanguage('ua')}><a href="#">Українська</a></li>
                        <li class="eng__lang" onClick={() => changeLanguage('en')}><a href="#">English</a></li>
                        <li class="pol__lang" onClick={() => changeLanguage('pl')}><a href="#">Polski</a></li>
                        <li class="deu__lang" onClick={() => changeLanguage('de')}><a href="#">Deutsch</a></li>
                      </ul>
                    </div>
                  </div>

                </div>
                <div class="header__burger">
                  <span></span>
                </div>
                <div class="menu__nav">
                  <ul class="menu__navigate">
                    <li><a href="#what">{t('WeCan')}</a></li>
                    <li><a href="#pro">{t('Projects')}</a></li>
                    <li><a href="#contacts">{t('Contacts')}</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </header>
        </div>
        <div class="container__iconslist main-wrapper">

          <img class="iconlist__redline" src={require('./img/lineRed.png')} alt="" />
          <div class="iconslist">
            <Modal onClose={this.showModal} show={this.state.show}>
            {t('SmartHouseText.1')} <strong class="color__blue">{t('SmartHouseText.2')}</strong> {t('SmartHouseText.3')} <strong class="color__blue">{t('SmartHouseText.4')} </strong> 
             {t('SmartHouseText.5')}
            </Modal>
            
            <p class="iconlist__decor_2">&#125;</p>
            <p class="big__title"><span class="dot__color">.</span>{t('WeCan')}<span class="iconlist__decor_1">&#123;</span></p>
            <div class="items__one">
              <img class="items__krest" src={require('./img/krest.png')} alt="" />
              <img class="items__krest-2" src={require('./img/krest.png')} alt="" />
              <img class="items__krest-3" src={require('./img/krest.png')} alt="" />
              <img class="items__line-blue" src={require('./img/lineBlue.png')} alt="" />
              <img class="items__line-red" src={require('./img/lineRed.png')} alt="" />
              <div class="iconslist__items_top">
                <div  class="iconslist__item reg__title">
                  <div id="cursor" class="item__back" /*onClick={e => { this.showModal(); }}*/>
                    <div class="item__front">
                     <a href="#pro"> <svg width="110" fill="#2155ee" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><clipPath><rect width="511.94" height="512"></rect></clipPath></defs><g><path d="M32.66,153.86a15,15,0,0,0-19.66,8,164.93,164.93,0,0,0,0,128.36,15,15,0,1,0,27.62-11.7,135,135,0,0,1,0-105A15,15,0,0,0,32.66,153.86Z"></path></g><path d="M479.34,153.86a15,15,0,0,0-8,19.66,135,135,0,0,1,0,105A15,15,0,1,0,499,290.18a164.93,164.93,0,0,0,0-128.36A15,15,0,0,0,479.34,153.86Z"></path><path d="M87.93,177.19a15,15,0,0,0-19.66,8,105,105,0,0,0,0,81.7,15,15,0,1,0,27.62-11.7,75.08,75.08,0,0,1,0-58.3A15,15,0,0,0,87.93,177.19Z"></path><path d="M424.07,177.19a15,15,0,0,0-8,19.66,75.08,75.08,0,0,1,0,58.3,15,15,0,1,0,27.62,11.7,105,105,0,0,0,0-81.7A15,15,0,0,0,424.07,177.19Z"></path><path d="M324.38,361H187.62a15,15,0,0,1-14.22-10.26L156.81,301h56.77a45,45,0,0,0,84.84,0h56.77l-16.58,49.74A15,15,0,0,1,324.38,361ZM241,391h30v91H241Zm30-105a15,15,0,1,1-15-15A15,15,0,0,1,271,286ZM177.61,43a15,15,0,0,1,14.87-13h127a15,15,0,0,1,14.87,13l24.78,228H298.42a45,45,0,0,0-84.84,0H152.83ZM390.06,277.52,364.2,39.66l0-.24A45.08,45.08,0,0,0,319.52,0h-127a45.08,45.08,0,0,0-44.65,39.42l0,.24L121.94,277.52a45.54,45.54,0,0,0,2,19.68l21,63A44.94,44.94,0,0,0,187.62,391H211v91H166a15,15,0,0,0,0,30H346a15,15,0,0,0,0-30H301V391h23.38a44.94,44.94,0,0,0,42.69-30.77l21-63A45.6,45.6,0,0,0,390.06,277.52Z"></path><path d="M226,91h60a15,15,0,0,0,0-30H226a15,15,0,0,0,0,30Z"></path><path d="M226,151h60a15,15,0,0,0,0-30H226a15,15,0,0,0,0,30Z"></path><path d="M196,211H316a15,15,0,0,0,0-30H196a15,15,0,0,0,0,30Z"></path></svg></a>
                    </div>
                  </div>
                  <p>{t('icon1.1')} <strong>{t('icon1.2')}</strong></p>
                </div>
                <div class="iconslist__item reg__title">
                  <div class="item__back">
                    <div class="item__front">
                      <svg width="110" fill="#2155ee" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 480"><defs><clipPath><rect width="511.94" height="479.94"></rect></clipPath></defs><g><path d="M120,90a30,30,0,1,1-30,30A30,30,0,0,1,120,90Zm0,90a60,60,0,1,0-60-60A60.06,60.06,0,0,0,120,180Z"></path><path d="M482,345a15,15,0,0,1-15,15H330V210H467a15,15,0,0,1,15,15Zm-212,0V225a15,15,0,0,1,15-15h15V360H285A15,15,0,0,1,270,345Zm-60-75h30v30H210ZM180,435a15,15,0,0,1-15,15H75a15,15,0,0,1-15-15V223.89a119.79,119.79,0,0,0,120,0ZM120,30a90,90,0,1,1-90,90A90.11,90.11,0,0,1,120,30ZM467,180H285a45.05,45.05,0,0,0-45,45v15H210V199.28a120,120,0,1,0-180,0V435a45.05,45.05,0,0,0,45,45h90a45.05,45.05,0,0,0,45-45V330h30v15a45.05,45.05,0,0,0,45,45H467a45.05,45.05,0,0,0,45-45V225A45.05,45.05,0,0,0,467,180Z"></path></g><circle cx="119.97" cy="315" r="15"></circle></svg>
                    </div>
                  </div>
                  <p>{t('icon2.1')} <strong>{t('icon2.2')}</strong></p>
                </div>
                <div class="iconslist__item reg__title">
                  <div class="item__back">
                    <div class="item__front">
                      <svg width="110" fill="#2155ee" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 450"><defs><style></style><clipPath><rect width="511.94" height="449.94"></rect></clipPath></defs><title>ethernet</title><g><path d="M482,405a15,15,0,0,1-15,15H45a15,15,0,0,1-15-15V45A15,15,0,0,1,45,30H467a15,15,0,0,1,15,15ZM467,0H45A45.05,45.05,0,0,0,0,45V405a45.05,45.05,0,0,0,45,45H467a45.05,45.05,0,0,0,45-45V45A45.05,45.05,0,0,0,467,0Z"></path></g><path d="M421,240H406a15,15,0,0,0-15,15v45H346a15,15,0,0,0-15,15v45H181V315a15,15,0,0,0-15-15H121V255a15,15,0,0,0-15-15H91V90H421ZM436,60H76A15,15,0,0,0,61,75V255a15,15,0,0,0,15,15H91v45a15,15,0,0,0,15,15h45v45a15,15,0,0,0,15,15H346a15,15,0,0,0,15-15V330h45a15,15,0,0,0,15-15V270h15a15,15,0,0,0,15-15V75A15,15,0,0,0,436,60Z"></path><path d="M106,360H76a15,15,0,0,0,0,30h30a15,15,0,0,0,0-30Z"></path><path d="M436,360H406a15,15,0,0,0,0,30h30a15,15,0,0,0,0-30Z"></path><path d="M136,210a15,15,0,0,0,15-15V135a15,15,0,0,0-30,0v60A15,15,0,0,0,136,210Z"></path><path d="M196,210a15,15,0,0,0,15-15V135a15,15,0,0,0-30,0v60A15,15,0,0,0,196,210Z"></path><path d="M256,210a15,15,0,0,0,15-15V135a15,15,0,0,0-30,0v60A15,15,0,0,0,256,210Z"></path><path d="M316,210a15,15,0,0,0,15-15V135a15,15,0,0,0-30,0v60A15,15,0,0,0,316,210Z"></path><path d="M376,210a15,15,0,0,0,15-15V135a15,15,0,0,0-30,0v60A15,15,0,0,0,376,210Z"></path></svg>
                    </div>
                  </div>
                  <p>{t('icon3.1')} <strong>{t('icon3.2')}</strong></p>
                </div>





              </div>
              <div class="iconslist__items_bot">

                <div class="iconslist__item reg__title">
                  <div class="item__back">
                    <div class="item__front">
                      <svg width="110" fill="#2155ee" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 511 511"><g class="cls-2"><path d="M481.06,420.18H29.94v-92.4a44.9,44.9,0,0,0,57.31-27.37h319a74.94,74.94,0,0,1,74.85,74.86Zm-29.94,60.88H59.88V450.11H451.12ZM44.91,270.47a15,15,0,1,1-15,15A15,15,0,0,1,44.91,270.47Zm361.3,0h-319a44.54,44.54,0,0,0-5.69-10.93L413.79,27.24A15,15,0,1,0,396.62,2.7l-341.74,239A44.87,44.87,0,0,0,0,285.44V435.15a15,15,0,0,0,15,15h15V496a15,15,0,0,0,15,15H466.09a15,15,0,0,0,15-15V450.12h15a15,15,0,0,0,15-15V375.27A104.91,104.91,0,0,0,406.21,270.47Z" transform="translate(0 0)"></path></g><path d="M74.85,330.35a15,15,0,0,0-15,15v29.95a15,15,0,0,0,29.94,0V345.32A15,15,0,0,0,74.85,330.35Z" transform="translate(0 0)"></path><path d="M134.73,330.35a15,15,0,0,0-15,15v29.95a15,15,0,1,0,29.94,0V345.32A15,15,0,0,0,134.73,330.35Z" transform="translate(0 0)"></path><path d="M270.47,165.68v59.88a15,15,0,0,0,29.94,0V165.68a15,15,0,0,0-29.94,0Z" transform="translate(0 0)"></path><path d="M330.36,165.68v59.88a15,15,0,1,0,29.94,0V165.68a15,15,0,1,0-29.94,0Z" transform="translate(0 0)"></path><path d="M405.21,150.71a15,15,0,0,0-15,15v59.88a15,15,0,0,0,29.94,0V165.68A15,15,0,0,0,405.21,150.71Z" transform="translate(0 0)"></path></svg>
                    </div>
                  </div>
                  <p>{t('icon4.1')} <strong>{t('icon4.2')}</strong></p>
                </div>
                <div class="iconslist__item reg__title">
                  <div class="item__back">
                    <div class="item__front">
                      <svg width="110" fill="#2155ee" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><g><path d="M406,301h15v30H406a15,15,0,0,0-15,15v30a15,15,0,0,1-15,15H346a15,15,0,0,0-15,15v15H301V406a15,15,0,0,0-15-15H226a15,15,0,0,0-15,15v15H181V406a15,15,0,0,0-15-15H136a15,15,0,0,1-15-15V346a15,15,0,0,0-15-15H91V301h15a15,15,0,0,0,15-15V226a15,15,0,0,0-15-15H91V181h15a15,15,0,0,0,15-15V136a15,15,0,0,1,15-15h30a15,15,0,0,0,15-15V91h30v15a15,15,0,0,0,15,15h60a15,15,0,0,0,15-15V91h30v15a15,15,0,0,0,15,15h30a15,15,0,0,1,15,15v30a15,15,0,0,0,15,15h15v30H406a15,15,0,0,0-15,15v60A15,15,0,0,0,406,301Zm30-60a15,15,0,0,0,15-15V211h46a15,15,0,0,0,0-30H451V166a15,15,0,0,0-15-15H421V136a45.05,45.05,0,0,0-45-45H361V76a15,15,0,0,0-15-15H331V15a15,15,0,0,0-30,0V61H286a15,15,0,0,0-15,15V91H241V76a15,15,0,0,0-15-15H211V15a15,15,0,0,0-30,0V61H166a15,15,0,0,0-15,15V91H136a45.05,45.05,0,0,0-45,45v15H76a15,15,0,0,0-15,15v15H15a15,15,0,0,0,0,30H61v15a15,15,0,0,0,15,15H91v30H76a15,15,0,0,0-15,15v15H15a15,15,0,0,0,0,30H61v15a15,15,0,0,0,15,15H91v15a45.05,45.05,0,0,0,45,45h15v15a15,15,0,0,0,15,15h15v46a15,15,0,0,0,30,0V451h15a15,15,0,0,0,15-15V421h30v15a15,15,0,0,0,15,15h15v46a15,15,0,0,0,30,0V451h15a15,15,0,0,0,15-15V421h15a45.05,45.05,0,0,0,45-45V361h15a15,15,0,0,0,15-15V331h46a15,15,0,0,0,0-30H451V286a15,15,0,0,0-15-15H421V241Z"></path></g><path d="M331,331H181V181h98.79L331,232.21ZM296.61,155.39A15,15,0,0,0,286,151H166a15,15,0,0,0-15,15V346a15,15,0,0,0,15,15H346a15,15,0,0,0,15-15V226a15,15,0,0,0-4.39-10.61Z"></path><path d="M211,286a15,15,0,0,0,15,15h60a15,15,0,0,0,0-30H226A15,15,0,0,0,211,286Z"></path><path d="M226,241h30a15,15,0,0,0,0-30H226a15,15,0,0,0,0,30Z"></path></svg>
                    </div>
                  </div>
                  <p>{t('icon5.1')}<br></br> <strong>{t('icon5.2')}</strong></p>
                </div>

              </div>



            </div>
          </div>
          {/* <div class="container__description">
            <div class="container__description__block reg__title">
              <p>
                {t('SmartHouseText.1')}<strong class="color__blue">{t('SmartHouseText.2')}</strong>{t('SmartHouseText.3')}<strong class="color__blue">{t('SmartHouseText.4')}</strong>{t('SmartHouseText.5')}
              </p>
            </div>

          </div> */}
        </div>
        <div class="container__benefits main-wrapper">

          <div class="benefits__text">
            <img class="benefits__img-1" src={require('./img/benefits.png')} alt="" />
            <img class="benefits__img-2" src={require('./img/benefits-2.png')} alt="" />
            <img class="benefits__img-3" src={require('./img/lineBlack.png')} alt="" />
            <div class="benefits__block">
              <p class="reg__title">
                {t('MainCriteria')}
              </p>
              <p class="big__title">
                {t('MainCriteriaText')}
              </p>
              <p class="reg__title">
                {t('MainDescription')}
              </p>
            </div>
          </div>
        </div>
        <div class="container__contact">
          <div class="contact__block">
            <p class="reg__title">
              {t('ContactContainerText')}
            </p>
            <button class="banner__btn"><a href="#contacts"> {t('ContactContainerButton')}</a></button>
          </div>
        </div>
        <div id="pro" class="container__projects">
          <div class="projects">
            <p class="banner__decor_1">&#123;</p>
            <p class="banner__decor_2">&#125;</p>
            <p class="big__title"><span class="dot__color">.</span>{t('CompanyProjects1')} <span class="dot__color">.</span>{t('CompanyProjects2')}</p>
            <div class="projects__items">
              <div class="projects__item">
                <div class="projects__img">
                  <img src={require('./img/airplane__project.png')} alt="" />
                </div>
                <div class="projects__text">
                  <p class="mid__title"><a href="#">{t('FlightPlan')}</a></p>
                  <p class="reg__title">{t('FlightPlanDescription')}</p>
                </div>
              </div>
              <div class="projects__item">
                <div class="projects__img">
                  <img src={require('./img/Ministerium.png')} alt="" />
                </div>
                <div class="projects__text">
                  <p class="mid__title"><a href="#">{t('MinisteriumAero')}</a></p>
                  <p class="reg__title">{t('MinisteriumAeroDescription')}</p>
                </div>
              </div>
            </div>
            <div class="projects__logos">
              <div class="projects__logo">
                <img src={require('./img/UIA.svg')} alt="" />
              </div>
              <div class="projects__logo">
                <img src={require('./img/Belavia_logo.svg')} alt="" />
              </div>
              <div class="projects__logo">
                <img src={require('./img/Czech_Airlines_Logo.svg')} alt="" />
              </div>
              <div class="projects__logo">
                <img src={require('./img/Turkish_Airlines_logo.svg')} alt="" />
              </div>

            </div>
            <div class="projects__logos">
              <div class="projects__logo">
                <img src={require('./img/Fly_Dubai_logo.svg')} alt="" />
              </div>
              <div class="projects__logo" id="az">
                <img src={require('./img/AZAL.svg')} alt="" />
              </div>
              <div class="projects__logo">
                <img src={require('./img/Odesa_airport_logo.svg')} alt="" />
              </div>

            </div>
          </div>
        </div>
        <footer id="contacts">
          <div class="container__form">
            <div class="container__contacttitle">
              <div class="container__contacttext">
                <p class="banner__decor_1">&#123;</p>
                <p class="banner__decor_2">&#125;</p>
                <p class="big__title"><span class="dot__color">.</span>{t('Write')}</p>
                <p class="reg__title">{t('WriteDescription')}
                </p>
              </div>
              <div class="form">
                <div class="form__block">
                  <input class="form__input" placeholder={t('NameQuestion')} type="text" onChange={this.handleChangeName} />
                  <input class="form__input" placeholder={t('Email')} type="email" onChange={this.handleChangeEmail} />
                  <input class="form__input" placeholder={t('PhoneNumber')} type="tel" onChange={this.handleChangePhone} />
                  <textarea id="textAreaStyle" class="form__input" placeholder={t('Message')} type="text" onChange={this.handleChangeText} />
                  <button class="banner__btn" onClick={this.handleSubmit}>{t('SendButton')}</button>
                </div>
              </div>
              <div class="clearfx"></div>
            </div>
          </div>

          <div class="footer__contacts">
            <p class="banner__decor_1">&#123;</p>
            <p class="banner__decor_2">&#125;</p>
            <p class="big__title"><span class="dot__color">.</span>{t('Contacts')}</p>
            <div class="footer__block">

              <div class="footer_phone">
                <p class="reg__title"><strong>{t('PhoneNumbers')}</strong></p>
                <p class="reg__title">
                  +38 (048) 705 99 89
                    </p>
              </div>
              <div class="footer__email">
                <p class="reg__title"><strong>{t('MyEmail')}</strong> </p>
                <p class="reg__title">info@profi-it.com</p>
              </div>
              <div class="footer__social">
                <p class="reg__title"><strong>{t('SocialNetworks')}</strong></p>
                <a href="#">
                  <svg fill='#2155ee' width='27px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><clipPath><rect width="511.94" height="512" /></clipPath></defs><g><path d="M352,0H160A160,160,0,0,0,0,160V352A160,160,0,0,0,160,512H352A160,160,0,0,0,512,352V160A160,160,0,0,0,352,0M464,352A112.12,112.12,0,0,1,352,464H160A112.12,112.12,0,0,1,48,352V160A112.12,112.12,0,0,1,160,48H352A112.12,112.12,0,0,1,464,160Z" /></g><path d="M256,128A128,128,0,1,0,384,256,128,128,0,0,0,256,128m0,208a80,80,0,1,1,80-80A80.11,80.11,0,0,1,256,336Z" /><path d="M393.6,101.34a17.06,17.06,0,1,1-17.06,17.06,17.06,17.06,0,0,1,17.06-17.06" /></svg>
                </a>
                <a href="#">
                  <svg fill='#2155ee' width='33px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12.8 10.4"><path d="M12.8,1.83a5.11,5.11,0,0,1-1.51.41A2.61,2.61,0,0,0,12.45.79a5.36,5.36,0,0,1-1.67.64,2.62,2.62,0,0,0-4.54,1.8,2.88,2.88,0,0,0,.06.59A7.46,7.46,0,0,1,.89,1.08,2.58,2.58,0,0,0,.54,2.4,2.61,2.61,0,0,0,1.7,4.58,2.6,2.6,0,0,1,.52,4.26v0a2.64,2.64,0,0,0,2.1,2.58A3.1,3.1,0,0,1,1.93,7a2.81,2.81,0,0,1-.5,0A2.64,2.64,0,0,0,3.89,8.73,5.24,5.24,0,0,1,.62,9.85c-.21,0-.41,0-.62,0A7.38,7.38,0,0,0,4,11,7.42,7.42,0,0,0,11.5,3.53V3.19A5.3,5.3,0,0,0,12.8,1.83Z" /></svg>
                </a>
                <a href="#">
                  <svg fill='#2155ee' width='27px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72"><path d="M54.5,43.15c-.22-.36-.79-.57-1.66-1s-5.12-2.51-5.92-2.8-1.37-.43-2,.43-2.24,2.8-2.74,3.37-1,.65-1.88.22a23.67,23.67,0,0,1-7-4.27,26.27,26.27,0,0,1-4.82-6c-.5-.86,0-1.33.38-1.75s.87-1,1.3-1.51a5.59,5.59,0,0,0,.87-1.44A1.55,1.55,0,0,0,31,27c-.21-.43-1.94-4.66-2.67-6.38s-1.44-1.44-1.95-1.44-1.08-.07-1.66-.07a3.22,3.22,0,0,0-2.31,1.08,9.62,9.62,0,0,0-3,7.17c0,4.22,3.11,8.31,3.54,8.88s6,9.54,14.8,13,8.81,2.29,10.4,2.14,5.12-2.07,5.85-4.08A7.11,7.11,0,0,0,54.5,43.15ZM36.66,5.59A29.63,29.63,0,0,0,6.94,35.07,29.17,29.17,0,0,0,12.6,52.36l-3.71,11,11.42-3.63A29.74,29.74,0,0,0,66.37,35.07,29.63,29.63,0,0,0,36.66,5.59ZM72,35.07A35.41,35.41,0,0,1,19.57,65.78L0,72,6.38,53.18A34.67,34.67,0,0,1,1.31,35.07a35.35,35.35,0,0,1,70.69,0Z" /></svg>
                </a>
                <a href="#">
                  <svg fill='#2155ee' width='27px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 408.79 408.79"><defs></defs><path d="M353.7.21H55.08A55.08,55.08,0,0,0,0,55.3V353.91A55.08,55.08,0,0,0,55.08,409H202.36l.25-146.08h-38a9,9,0,0,1-8.95-8.92l-.19-47.09a9,9,0,0,1,9-9h37.88V152.43c0-52.8,32.25-81.55,79.35-81.55h38.65a9,9,0,0,1,8.95,9v39.71a9,9,0,0,1-8.95,9H296.64c-25.61,0-30.57,12.17-30.57,30v39.39h56.29a9,9,0,0,1,8.89,10L325.67,255a9,9,0,0,1-8.9,7.9H266.32L266.07,409H353.7a55.09,55.09,0,0,0,55.09-55.08V55.3A55.1,55.1,0,0,0,353.7.21" /></svg>
                </a>
              </div>
            </div>
          </div>
          <div class="footer__container">
            <div class="footer__width">
              <div class="footer__logo">
                <img class="big__title" src={require('./img/Profi-IT_logo.png')} alt="" />
                <p class="reg__title">© <strong>Profi-it</strong>{t('Rights')}</p>
              </div>
            </div>
          </div>
          
        </footer>

    </body>
    );
  }
}

export default withNamespaces()(App);
