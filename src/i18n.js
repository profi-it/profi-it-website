import i18n from "i18next";
import { reactI18nextModule } from "react-i18next";

import translationEN from './locales/en/translation.json';
import translationDE from './locales/de/translation.json';
import translationRU from './locales/ru/translation.json';

// the translations
const resources = {
  en: {
    translation: translationEN
  },
  de:{
      translation:translationDE
  },
  ru:{
      translation:translationRU
  }
};
i18n
  .use(reactI18nextModule) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "ru",

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;